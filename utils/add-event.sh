#!/bin/bash
set -xe
TITLE=$1
DATE=$2
HOUR=$3
LIMIT=$4
if [ -z "$TITLE" ] || [ -z "$DATE" ] || [ -z "$HOUR" ] || [ -z "$LIMIT" ]
then
	echo "Usage: add-event <TITLE> <DATE> <HOUR> <LIMIT>"
	exit 1;
fi

#example:
# ./add-event.sh "Reuniao de Sábado (Ceia)" "2021-01-30" "19:00:00" "70"

sudo mysql -u root -e "insert into igreja.evento (title,data,hora,limite) VALUES (\"$TITLE\",\"$DATE\",\"$HOUR\",$LIMIT);"
