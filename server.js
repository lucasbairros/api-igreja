const http         = require('http')
const Router       = require('router')
const finalhandler = require('finalhandler')
const compression  = require('compression')
const bodyParser   = require('body-parser')
const winston      = require('winston')
const DatabaseConnector     = require('./database')

const DEFAULT_DATABASE = 'igreja';
const DEFAULT_ROUTE_PATH = '/api/public/api';

const logger = winston.createLogger({
  level: 'info',
  transports: [
    new winston.transports.File({ filename: 'error.log', level: 'error' }),
    new winston.transports.File({ filename: 'api-igreja.log' }),
  ],
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.combine(
      winston.format.label('[api]'),
      winston.format.timestamp({
        format: 'YYYY-MM-DD HH:mm:ss',
      }),
      winston.format.colorize(),
      winston.format.printf(({ level,
        message,
        label,
        timestamp }) => `${timestamp} ${level}: ${message}`),
    )}));
}

var router = Router()
var server = http.createServer(function onRequest(req, res) {
  router(req, res, finalhandler(req, res))
})

router.use(compression())


var api = Router()
router.use(process.env.API_IGREJA_DEFAULT_ROUTE_PATH
    || DEFAULT_ROUTE_PATH, api);

api.use(bodyParser.json())

const database = new DatabaseConnector();

database.init(process.env.API_IGREJA_DB_HOST,process.env.API_IGREJA_DB_USER,
  process.env.API_IGREJA_DB_PASS, process.env.API_IGREJA_DATABASE
  || DEFAULT_DATABASE);

api.get('/', async function (req, res) {
  res.statusCode = 200
  res.setHeader('Content-Type', 'application/json')

  const data = await database.getAllEventData() || [];

  res.end(JSON.stringify(data));
})

api.post('/form_add', async function (req, res) {
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/plain; charset=utf-8');
  logger.info('Adding new subscription: ' + JSON.stringify(req.body));
  const result = !! await database.add(req.body);
  logger.info('Subscription added: ' + result);
  res.end(result + '\n')
})

api.post('/form_add_agenda', async function (req, res) {
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/plain; charset=utf-8');
  logger.info('Adding new subscription (admin): ' + JSON.stringify(req.body));
  const result = !! await database.add(req.body);
  logger.info('Subscription added: ' + result);
  res.end(result + '\n')
})

api.post('/form_add_evento', async function (req, res) {
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/plain; charset=utf-8');
  logger.info('Adding new event: ' + JSON.stringify(req.body));
  const result = !! await database.add_event(req.body);
  logger.info('Event added: ' + result);
  res.end(result + '\n')
})

api.delete('/remove_evento/:id', async function (req, res) {
  const { id } = req.params;
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/plain; charset=utf-8')

  logger.info('Removing event: ' + id);

  let result;

  if (id) {
    params = req.body.value
    result = !! await database.remove_event(id);
    res.end (result + '\n');
  } else {
    result = false;
    res.end(false + '\n');
  }

  logger.info('Event removed: ' + result);
})

api.delete('/remove_lista/:id', async function (req, res) {
  const { id } = req.params;
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/plain; charset=utf-8')

  logger.info('Removing subscription: ' + id);

  let result;

  if (id) {
    params = req.body.value
    result = !! await database.remove(id);
    res.end (result + '\n');
  } else {
    result = false;
    res.end(false + '\n');
  }

  logger.info('Subscription removed: ' + result);
})

api.delete('/remove_agenda/:id', async function (req, res) {
  const { id } = req.params;
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/plain; charset=utf-8')

  logger.info('Removing subscription (admin): ' + id);

  let result;

  if (id) {
    params = req.body.value
    result = !! await database.remove(id);
    res.end (result + '\n');
  } else {
    result = false;
    res.end(false + '\n');
  }

  logger.info('Subscription removed: ' + result);
})

// make our http server listen to connections
server.listen(process.env.API_IGREJA_LISTEN_PORT || 3000)
logger.info('Application is running');
