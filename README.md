# simple api for igreja

## Install steps (debian-based systems)
### 1 Install deps
```bash
sudo apt-get install npm nodejs mysql-server

```

### 2 Init database
```sql
  cd api-igreja
  sudo mysql -u root
  source utils/db.sql

  # REPLACE USER and PASSWORD with your desired credentials
  create user 'USER'@'localhost' identified with mysql_native_password BY 'PASSWORD';
  grant all privileges on igreja.* to 'USER'@'localhost';

```

### 3 Install node dependencies
```
  cd api-igreja
  npm install
```

### 4 Run
```
  env API_IGREJA_USER=USER API_IGREJA_PASS=PASSWORD npm start
```
This will start application on default URL: http://localhost:3000/api/public/api

### 5 Enviroment variables
```
API_IGREJA_DB_HOST
API_IGREJA_DB_USER
API_IGREJA_DB_PASS
API_IGREJA_DATABASE
API_IGREJA_LISTEN_PORT
API_IGREJA_DEFAULT_ROUTE_PATH
```
